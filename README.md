This repository is made to store different images that can be used for building in other repo.

For now, we have the rust image with some basic components usefull for rust on gitlab ci :
 - fmt
 - clippy
 - cargo audit
 - cargo tarpaulin
 - sccache

It has been made initially for "Music-Stalker" repository.
